#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/PartonicTops.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/ChargedLeptons.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/AnalysisLoader.hh"
#include <fstream>
#include <string>

namespace Rivet {

  typedef std::map<std::string, Histo1DPtr> HistoMap1D;
  typedef std::map<std::string, Histo2DPtr> HistoMap2D;
  typedef std::map<std::string, std::string> StringDict;

  class ttbar_Dilepton_SpinDensity : public Analysis {
  public:

    // Anti-kT jet clustering radius parameter (not in use for parton level top observables)
    double jet_R = 0.4;
    // dressed lepton clustering radius parameter
    double dressed_lep_R = 0.1;

    // acceptance cuts (not used for parton-level distributions)

    // pseudorapidity
    double eta_max_full = 1000.0;
    double eta_max_l = 1000.0;
    double eta_max_j = 1000.0;

    // pT
    // all
    double pT_min_full = 0.0*GeV;
    // lep
    double pT_min_l = 0.0*GeV;
    // jet
    double pT_min_j = 0.0*GeV;

    // jet/lepton isolation radius
    double deltaR_min_j_l = 0.0;

    // missing ET cut
    double missingET_min = 0.0*GeV;

    // b-tagging efficiency and mistag probability
    double b_tag_eff = 1.0;
    double b_mistag_prob = 0.00;

    // max eta, min pT cut
    Cut cuts_full = Cuts::abseta < eta_max_full && Cuts::pT >= pT_min_full;

    // Lepton cuts
    Cut cuts_lep = Cuts::abseta < eta_max_l && Cuts::pT >= pT_min_l;
    // Jet cuts
    Cut cuts_j = Cuts::abseta < eta_max_j && Cuts::pT >= pT_min_j;

    // count negatively weighted events
    int n_neg = 0;

    /// Minimal constructor
    ttbar_Dilepton_SpinDensity() : Analysis("ttbar_Dilepton_SpinDensity")
    {
    }


    /// @name Analysis methods
    //@{

    /// Set up projections, book histograms etc.
    void init() {

      // track no. events
      //int this_event = 1;

      // ~~~~~~~~~~~~~~~~~~~~~~
      // Declare projections
      // ~~~~~~~~~~~~~~~~~~~~~~

      // FinalState used to select all particles with no pT/eta cut
      const FinalState fs; // no arg => defaults to fs(-MAXDOUBLE, MAXDOUBLE, 0*GeV) or fs(Cuts::open())

      // extract bare charged leptons from final state
      ChargedLeptons bare_leptons(fs);
      // form these into dressed leptons (include photon radiation in cone of R=0.1)
      // these should be identical at parton level anyway
      DressedLeptons leptons(fs, bare_leptons, dressed_lep_R, Cuts::open());
      // declare leptons projection
      declare(leptons, "leptons");

      // declare MET projection (won't use for extracting parton-level distributions)
      declare(MissingMomentum(fs), "missing_ET");

      // Form a final state with vetoes on the identified dressed leptons which pass cuts above + neutrinos for jet clustering
      // (won't use for extracting parton-level distributions)
      VetoedFinalState vfs(fs);
      vfs.vetoNeutrinos();
      vfs.addVetoOnThisFinalState(leptons);
      // declare jet projection
      declare(FastJets(vfs, FastJets::ANTIKT, jet_R), "jets");

      // Parton-level top quarks
      // false rejects ts which decay to e/mu via prompt tauonic W mode
      declare(PartonicTops(PartonicTops::E_MU, false), "leptonic_top_partons");

      // ~~~~~~~~~~~~~~~~~~~~~~
      //Random number for simulation of b-tagging efficiency and mistag probability
      srand (time(NULL));
      // ~~~~~~~~~~~~~~~~~~~~~~

      // ~~~~~~~~~~~~~~~~~~~~~~
      // hist binning parameters
      // ~~~~~~~~~~~~~~~~~~~~~~

      // mttbar limits
      double mttbar_min = 0;
      double mttbar_max = 4000;

      // pT limits
      double pT_t_max = 3000;
      double pT_tt_max = 2000;

      // cosine limits
      double c0_min = -1.0;
      double c0_max = 1.0;

      // xi limits
      double xi_min = -1.0;
      double xi_max = 1.0;

      // number of bins
      int nbins_theta = 10;
      int nbins_xi = 10;
      int nbins_mttbar = 40;

      // ~~~~~~~~~~~~~~~~~~~~~~
      // Booking of histograms
      // ~~~~~~~~~~~~~~~~~~~~~~

      // Booking of histograms

      // delta phi (leptons)
      _histos["dphi_ll"] = bookHisto1D("delta_phi_l+_l-", nbins_theta, 0, M_PI, "delta phi (l+ l-)" );

      // top kinematic distributions
      _histos["mtt"] = bookHisto1D("m_ttbar", nbins_mttbar, mttbar_min, mttbar_max);

      // transverse momenta
      _histos["t_1_pT"] = bookHisto1D("pT_t", logspace(40, 1., pT_t_max));
      _histos["t_2_pT"] = bookHisto1D("pT_tbar", logspace(40, 1., pT_t_max));
      _histos["tt_pT"] = bookHisto1D("pT_ttbar", logspace(40, 1., pT_tt_max));
      // rapidity
      _histos["y_t"] = bookHisto1D("y_t", 50, -2.5, 2.5);
      _histos["y_tt"] = bookHisto1D("y_ttbar", 50, -2.5, 2.5);

      // Spin observables

      // 1D uniangular for polarization

      // raw
      _histos_pol["c0p_n"] = bookHisto1D("dxsec_dcos0_n_t", nbins_theta, c0_min, c0_max, "cos theta+ (n)" );
      _histos_pol["c0m_n"] = bookHisto1D("dxsec_dcos0_n_tbar", nbins_theta, c0_min, c0_max, "cos theta- (n)" );
      _histos_pol["c0p_r"] = bookHisto1D("dxsec_dcos0_r_t", nbins_theta, c0_min, c0_max, "cos theta+ (r)" );
      _histos_pol["c0m_r"] = bookHisto1D("dxsec_dcos0_r_tbar", nbins_theta, c0_min, c0_max, "cos theta- (r)" );
      _histos_pol["c0p_k"] = bookHisto1D("dxsec_dcos0_k_t", nbins_theta, c0_min, c0_max, "cos theta+ (k)" );
      _histos_pol["c0m_k"] = bookHisto1D("dxsec_dcos0_k_tbar", nbins_theta, c0_min, c0_max, "cos theta- (k)" );
      _coeff_names["c0p_n"] = "B(n)+";
      _coeff_names["c0m_n"] = "B(n)-";
      _coeff_names["c0p_r"] = "B(r)+";
      _coeff_names["c0m_r"] = "B(r)-";
      _coeff_names["c0p_k"] = "B(k)+";
      _coeff_names["c0m_k"] = "B(k)-";
      //_h_c0p_rs = bookHisto1D("dxsec_dcos0_rstar_t", nbins_theta, c0_min, c0_max, "cos theta+ (r*)" );
      //_h_c0m_rs = bookHisto1D("dxsec_dcos0_rstar_tbar", nbins_theta, c0_min, c0_max, "cos theta- (r*)" );
      //_h_c0p_ks = bookHisto1D("dxsec_dcos0_kstar_t", nbins_theta, c0_min, c0_max, "cos theta+ (k*)" );
      //_h_c0m_ks = bookHisto1D("dxsec_dcos0_kstar_tbar", nbins_theta, c0_min, c0_max, "cos theta- (k*)" );

      // 1D - xi angle products for spin corrs

      //diagonal
      _histos_spincorr["xi_n_n"] = bookHisto1D("dxsec_dxi_n_n", nbins_xi, xi_min, xi_max, "xi_n_n" );
      _histos_spincorr["xi_r_r"] = bookHisto1D("dxsec_dxi_r_r", nbins_xi, xi_min, xi_max, "xi_r_r" );
      //_h_xi_rs_rs = bookHisto1D("dxsec_dxi_rstar_rstar", nbins_xi, xi_min, xi_max, "xi_r*_r*" );
      _histos_spincorr["xi_k_k"] = bookHisto1D("dxsec_dxi_k_k", nbins_xi, xi_min, xi_max, "xi_k_k" );
      //_h_xi_ks_ks = bookHisto1D("dxsec_dxi_kstar_kstar", nbins_xi, xi_min, xi_max, "xi_k*_k*" );
      _coeff_names["xi_n_n"] = "C(n,n)";
      _coeff_names["xi_r_r"] = "C(r,r)";
      _coeff_names["xi_k_k"] = "C(k,k)";

      // sums/differences
      // off-diag
      _histos_spincorr["xi_sum_r_k"] = bookHisto1D("dxsec_dxi_r_k_sum", nbins_xi, xi_min, xi_max, "xi_r_k + xi_k_r" );
      _histos_spincorr["xi_diff_r_k"] = bookHisto1D("dxsec_dxi_r_k_diff", nbins_xi, xi_min, xi_max, "xi_r_k - xi_k_r" );
      _histos_spincorr["xi_sum_n_r"] = bookHisto1D("dxsec_dxi_n_r_sum", nbins_xi, xi_min, xi_max, "xi_n_r + xi_r_n" );
      _histos_spincorr["xi_diff_n_r"] = bookHisto1D("dxsec_dxi_n_r_diff", nbins_xi, xi_min, xi_max, "xi_n_r - xi_r_n" );
      _histos_spincorr["xi_sum_n_k"] = bookHisto1D("dxsec_dxi_n_k_sum", nbins_xi, xi_min, xi_max, "xi_n_k + xi_k_n" );
      _histos_spincorr["xi_diff_n_k"] = bookHisto1D("dxsec_dxi_n_k_diff", nbins_xi, xi_min, xi_max, "xi_n_k - xi_k_n" );
      _coeff_names["xi_sum_r_k"] = "C(r,k)+C(k,r)";
      _coeff_names["xi_diff_r_k"] = "C(r,k)-C(k,r)";
      _coeff_names["xi_sum_n_r"] = "C(n,r)+C(r,n)";
      _coeff_names["xi_diff_n_r"] = "C(n,r)-C(r,n)";
      _coeff_names["xi_sum_n_k"] = "C(n,k)+C(k,n)";
      _coeff_names["xi_diff_n_k"] = "C(n,k)-C(k,n)";

      // open file for writing out asymmetry coefficients
      outfile.open ("./coeffs.dat", ios::trunc);

      }

      // ~~~~~~~~~~~~~~~~~~~~~~
      // define sign function
      // ~~~~~~~~~~~~~~~~~~~~~~

      template <typename T> int sgn(T val) {
        return (T(0) < val) - (val < T(0));
      }

      // ***********************************
      // ***********************************
      //       main analysis function
      // ***********************************
      // ***********************************

      void analyze(const Event& event) {

        // get event weight
        const double weight = event.weight();

        // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //      get tops from event record
        // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        // leptonically decaying tops
        const Particles& leptonicpartontops = apply<ParticleFinder>(event, "leptonic_top_partons").particlesByPt();
        Particles chargedleptons;
        unsigned int ntrueleptonictops = 0;
        bool oppositesign = false;

        if ( leptonicpartontops.size() == 2 ) {
          for (size_t k = 0; k < leptonicpartontops.size(); ++k) {

            // Get the lepton
            const Particle lepTop = leptonicpartontops[k];
            const auto isPromptChargedLepton = [](const Particle& p){return (isChargedLepton(p) && isPrompt(p, false, false));};
            Particles lepton_candidates = lepTop.allDescendants(firstParticleWith(isPromptChargedLepton), false);
            if ( lepton_candidates.size() < 1 ) MSG_WARNING("error, PartonicTops::E_MU top quark had no daughter lepton candidate, skipping event.");

            // In some cases there is no lepton from the W decay but only leptons from the decay of a radiated gamma.
            // These hadronic PartonicTops are currently being mistakenly selected by PartonicTops::E_MU (as of April 2017), and need to be rejected.
            // PartonicTops::E_MU is being fixed in Rivet, and when it is the veto below should do nothing.
            /// @todo Should no longer be necessary -- remove
            bool istrueleptonictop = false;
            for (size_t i = 0; i < lepton_candidates.size(); ++i) {
              const Particle& lepton_candidate = lepton_candidates[i];
              if ( lepton_candidate.hasParent(PID::PHOTON) ) {
                MSG_DEBUG("Found gamma parent, top: " << k+1 << " of " << leptonicpartontops.size() << " , lepton: " << i+1 << " of " << lepton_candidates.size());
                continue;
              }
              if ( !istrueleptonictop && sameSign(lepTop,lepton_candidate) ) {
                chargedleptons.push_back(lepton_candidate);
                istrueleptonictop = true;
              }
              else MSG_WARNING("Found extra prompt charged lepton from top decay (and without gamma parent), ignoring it.");
            }
            if ( istrueleptonictop ) ++ntrueleptonictops;
          }
        }

        if ( ntrueleptonictops == 2 ) {
          oppositesign = !( sameSign(chargedleptons[0],chargedleptons[1]) );
          if ( !oppositesign ) MSG_WARNING("error, same charge tops, skipping event.");
        }

        // if identify two tops and two oppositely charged leptons, calculate the observables
        if ( ntrueleptonictops == 2 && oppositesign ) {

          // track negatively weighted events
          if (weight < 0.0) {
            cout << "NEGATIVE WEIGHT" << endl;
            n_neg += 1;
            //vetoEvent;
          }

          // Get the four-momenta of the positively- and negatively-charged leptons
          FourMomentum lep_plus_momentum = chargedleptons[0].charge() > 0 ? chargedleptons[0] : chargedleptons[1];
          FourMomentum lep_minus_momentum = chargedleptons[0].charge() > 0 ? chargedleptons[1] : chargedleptons[0];

          // Get the four-momenta of the positively- and negatively-charged tops
          FourMomentum t_momentum = leptonicpartontops[0].pdgId() > 0 ? leptonicpartontops[0] : leptonicpartontops[1];
          FourMomentum tbar_momentum = leptonicpartontops[0].pdgId() > 0 ? leptonicpartontops[1] : leptonicpartontops[0];
          FourMomentum ttbar_momentum = t_momentum + tbar_momentum;





          // *************************** Observables ******************************

          // delta phi leptons
          double deltaphi_l_l = deltaPhi(lep_plus_momentum.phi(), lep_minus_momentum.phi());

          // calculate ttbar invariant mass
          double mttrec = ttbar_momentum.mass()*GeV;

          // pT of tt system
          double pttt = (t_momentum + tbar_momentum).pt()*GeV;

          // y_t
          double y_t = t_momentum.rapidity();
          // y_tt~
          double y_tt = ttbar_momentum.rapidity();


          // top kinematic distributions

          // fill mtt~ spectrum,
          _histos["mtt"]->fill(mttrec/GeV, weight);
          // t, t~ pT
          _histos["t_1_pT"]->fill(t_momentum.pT()/GeV, weight);
          _histos["t_2_pT"]->fill(tbar_momentum.pT()/GeV, weight);
          // pT (tt~)
          _histos["tt_pT"]->fill(pttt/GeV, weight);
          // y_t
          _histos["y_t"]->fill(y_t, weight);
          // y_tt
          _histos["y_tt"]->fill(y_tt, weight);

          // spin-sensitive observables

          // fill dphi l+l- histo
          _histos["dphi_ll"]->fill(deltaphi_l_l,weight);

          // calculate unit vector the direction along which we will boost, for initial states
          Vector3 tt_ZMF_boostdir = (ttbar_momentum.vector3()).unit();

          // project out 3 momentum of this and calculate boost matrix:
          //LorentzTransform tt_ZMF_boost(-1 * psum_tt.boostVector()); //original
          LorentzTransform tt_ZMF_boost = LorentzTransform::mkFrameTransformFromBeta(ttbar_momentum.betaVec());
          //LorentzTransform tt_ZMF_boost = LorentzTransform(-1 * psum_tt.boostVector());

          // boost total four momenta to zmf to check
          FourMomentum psum_tt_ZMF = tt_ZMF_boost.transform(ttbar_momentum);

          // boost everything in FS we need to this frame:

          //leps
          FourMomentum lplus_tt_ZMF = tt_ZMF_boost.transform(lep_plus_momentum);
          FourMomentum lminus_tt_ZMF = tt_ZMF_boost.transform(lep_minus_momentum);
          //tops
          FourMomentum t_tt_ZMF = tt_ZMF_boost.transform(t_momentum);
          FourMomentum tbar_tt_ZMF = tt_ZMF_boost.transform(tbar_momentum);

          // pp^: unit vector along +ve z for initial parton in lab frame:
          double ppx=0.0;
          double ppy=0.0;
          double ppz=1.0;

          //Vector3 pp_vec = Vector3(0.0,0.0,1.0);
          Vector3 pp_vec;
          pp_vec.setX(ppx);
          pp_vec.setY(ppy);
          pp_vec.setZ(ppz);

          // k^: top in zmf
          Vector3 k_vec = (t_tt_ZMF.vector3()).unit();

          // yp: component of t dir in zmf along z dir
          double yp = pp_vec.dot(k_vec);

          // rp
          double rp = sqrt( 1.0-(yp*yp) );

          // rp^
          Vector3 rp_vec = (1/rp)*(pp_vec - yp*k_vec);

          // np^
          Vector3 np_vec = (1/rp)*(pp_vec.cross(k_vec));

          // define some kinematic quantities for feeding the analytic expressions for checking how cuts sculpt distributions?
          //double sqrts = psum_tt_ZMF.mass();
          //double z = 2*mt/sqrts;
          //double B = sqrt(1-pow(z,2));
          // use these and yp in functions for b coeffs if we want to check those

          // calculate boost matrices:
          //LorentzTransform t_ZMF_boost(-1 * t_tt_ZMF.boostVector()); //original
          //LorentzTransform tbar_ZMF_boost(-1 * tbar_tt_ZMF.boostVector()); //original
          LorentzTransform t_ZMF_boost = LorentzTransform::mkFrameTransformFromBeta(t_tt_ZMF.betaVec());
          LorentzTransform tbar_ZMF_boost = LorentzTransform::mkFrameTransformFromBeta(tbar_tt_ZMF.betaVec());
          //LorentzTransform t_ZMF_boost = LorentzTransform(-1 * t_tt_ZMF.boostVector());
          //LorentzTransform tbar_ZMF_boost = LorentzTransform(-1 * tbar_tt_ZMF.boostVector());

          // boost leptons to their respective parent tops' frames
          FourMomentum lplus_t_ZMF = t_ZMF_boost.transform(lplus_tt_ZMF);
          FourMomentum lminus_tbar_ZMF = tbar_ZMF_boost.transform(lminus_tt_ZMF);

          // Extract unit vectors for their three momenta
          Vector3 lplus_vec = (lplus_t_ZMF.vector3()).unit();
          Vector3 lminus_vec = (lminus_tbar_ZMF.vector3()).unit();

          // now define our five choices of the reference axes a^ and b^
          // and the corresponding angles  for each, c0(+/-) = cos theta (+/-) = l^ (+/-) . (a^/b^) respectively

          // transverse: n
          Vector3 an_vec = sgn(yp) * np_vec;
          Vector3 bn_vec = -sgn(yp) * np_vec;
          double c0plus_n = lplus_vec.dot(an_vec);
          double c0minus_n = lminus_vec.dot(bn_vec);

          // r axis:     r
          Vector3 ar_vec = sgn(yp) * rp_vec;
          Vector3 br_vec = -sgn(yp) * rp_vec;
          double c0plus_r = lplus_vec.dot(ar_vec);
          double c0minus_r = lminus_vec.dot(br_vec);

          // helicity:   k
          Vector3 ak_vec = k_vec;
          Vector3 bk_vec = -k_vec;
          double c0plus_k = lplus_vec.dot(ak_vec);
          double c0minus_k = lminus_vec.dot(bk_vec);



          // angle product vars for spin corrs:

          // nn
          double xi_n_n = c0plus_n * c0minus_n;

          // nr, nr*
          double xi_n_r = c0plus_n * c0minus_r;


          // nk, nk*
          double xi_n_k = c0plus_n * c0minus_k;


          // rn, r*n
          double xi_r_n = c0plus_r * c0minus_n;


          // rr, r*r*
          double xi_r_r = c0plus_r * c0minus_r;


          // rk, r*k*
          double xi_r_k = c0plus_r * c0minus_k;


          // kn, k*n
          double xi_k_n = c0plus_k * c0minus_n;


          // kr, k*r*
          double xi_k_r = c0plus_k * c0minus_r;


          // kk, k*k*
          double xi_k_k = c0plus_k * c0minus_k;

          // fill these histograms

          // Observables - fill histograms:

          //  uniangular for polarization
          // one each for the angles cos0(+ & -) for each choice of the spin ref axes ( a, b = {r,k,n} )

          // raw
          _histos_pol["c0p_n"]->fill(c0plus_n, weight);
          _histos_pol["c0m_n"]->fill(c0minus_n, weight);
          _histos_pol["c0p_r"]->fill(c0plus_r, weight);
          _histos_pol["c0m_r"]->fill(c0minus_r, weight);
          _histos_pol["c0p_k"]->fill(c0plus_k, weight);
          _histos_pol["c0m_k"]->fill(c0minus_k, weight);

          //  xi angle products for spin corrs
          //diagonal
          // raw
          _histos_spincorr["xi_n_n"]->fill(xi_n_n, weight);
          _histos_spincorr["xi_r_r"]->fill(xi_r_r, weight);
          _histos_spincorr["xi_k_k"]->fill(xi_k_k, weight);

          // sums/differences
          // off-diag
          _histos_spincorr["xi_sum_r_k"]->fill(xi_r_k + xi_k_r, weight);
          _histos_spincorr["xi_diff_r_k"]->fill(xi_r_k - xi_k_r, weight);
          _histos_spincorr["xi_sum_n_r"]->fill(xi_n_r + xi_r_n, weight);
          _histos_spincorr["xi_diff_n_r"]->fill(xi_n_r - xi_r_n, weight);
          _histos_spincorr["xi_sum_n_k"]->fill(xi_n_k + xi_k_n, weight);
          _histos_spincorr["xi_diff_n_k"]->fill(xi_n_k - xi_k_n, weight);

        }

        //this_event += 1;

      }

      // ***********************************
      // ***********************************
      //  END main analysis function
      // ***********************************
      // ***********************************

      void finalize() {

        cout << "~~ Finished main event loop! ~~" << endl;
        cout << "~~ Debug: no. negative weights: " << n_neg << endl;
        cout << "~~ Calculating spin density coefficients...  ~~" << endl;

        // normalise regular histograms
        foreach(HistoMap1D::value_type &histo, _histos) {

          // norm
          normalize(histo.second);
          //scale(histo.second, crossSection()/sumOfWeights() );

        }

        // write header for coeffs to file
        outfile << "coeff:  value:  MC err:\n";

        // normalise spin density histograms, calculate asymmetry coefficients
        foreach(HistoMap1D::value_type &histo, _histos_pol) {

          //double asym = 0, err = 0;

          // norm
          normalize(histo.second);

          // normalisation factor relating mean histogram value to coefficients
          double norm = 3.0;
          // mean x
          double mean = histo.second->xMean();//1.0;
          // error on mean
          double err = histo.second->xStdErr();// 1.0;

          cout << "Observable: " << histo.first << endl;

          // write spin density coeff to file
          outfile << _coeff_names[histo.first] << ", " << norm * mean << ", " << norm * err << "\n";

        }

        // normalise spin density histograms, calculate asymmetry coefficients
        foreach(HistoMap1D::value_type &histo, _histos_spincorr) {

          //double asym = 0, err = 0;

          // norm
          normalize(histo.second);

          // normalisation factor relating mean histogram value to coefficients
          double norm = -9.0;
          // mean x
          double mean = histo.second->xMean();//1.0;
          // error on mean
          double err = histo.second->xStdErr();//1.0;

          cout << "Observable: " << histo.first << endl;

          // write spin density coeff to file
          outfile << _coeff_names[histo.first] << ", " << norm * mean << ", " << norm * err << "\n";

        }

        // close file once asymmetries calculated and written out
        outfile.close();

        cout << "~~~  Finished! ~~~~" << endl;
        cout << "~~  ¯\\_(ツ)_/¯  ~~" << endl;

      }

  private:

    // declare output stream for writing asymmetry coefficients to file
    ofstream outfile;

    // container for multiple histograms
    HistoMap1D _histos, _histos_pol, _histos_spincorr;
    StringDict _coeff_names;

    //int this_event;
  };

  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(ttbar_Dilepton_SpinDensity);

}
