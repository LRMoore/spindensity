# /usr/bin/env/python
# -*- coding: utf-8 -*-

"""
Run Herwig 7 on a parton-level .lhe file without showering to convert to .hepmc
"""
from __future__ import print_function
import sys
import os
import regex as re
import shutil
import argparse
import subprocess


##### functions:

#
## get absolute paths to files/directories in directory argument (which optionally match a pattern)
def getAbsolutePaths(directory, include_dirs=True, include_files=True, dir_pattern='.*',file_pattern='.*'):

    # initialise lists to hold directories and files
    matching_dirs=[]
    matching_files=[]

    # accumulate absolute paths of files and directories within target dir
    for dir_path,dir_name,file_name in os.walk(directory):
            if include_dirs == True:
                abs_dir_paths = map( lambda x: os.path.abspath(os.path.join(dir_path,x)), dir_name)
                matching_dirs.extend(abs_dir_paths)
            if include_files == True:
                abs_file_paths = map( lambda x: os.path.abspath(os.path.join(dir_path,x)), file_name)
                matching_files.extend(abs_file_paths)

    # filter down directories and files to those which match patterns if specified
    matching_dirs = filter(lambda x: re.match(dir_pattern,x), matching_dirs)
    matching_files = filter(lambda x: re.match(file_pattern,x), matching_files)

    return matching_dirs + matching_files

##
#

# flexible print to stderr
def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def validateFile(my_file):
    if not os.path.isfile(my_file):
        eprint("{} is not an existing file, exiting...".format(my_file))
        sys.exit(1)



####
# Globals:

# using input lhe file, use header to isolate banner and derive other params
lhe_banner_pattern = re.compile("(<header>(.|\n|\r|(\r|\n))*?</header>)+?")
# param patterns we need to read from lhe
# PDF:
pdf_name_pattern = '(\w+)\s+=\s(pdlabel)'
pdf_num_pattern = '(\w+)\s+=\s(lhaid)'
# num unw events:
num_evts_pattern = '([0-9]+)\s+=\s(nevents)'


# ----- command line options for script

parser = argparse.ArgumentParser(description=__doc__)

user = parser.add_mutually_exclusive_group()

# path to lhe
parser.add_argument('lhe_path', type=str, help='Path to .lhe file')

# verbose mode
parser.add_argument("-v","--verbose", dest="verbose", action="store_true",default=False)

# switch to run herwig or just produce card
parser.add_argument("-r","--run-herwig", dest="run_herwig", action="store_true", default=False)

args = parser.parse_args()
lhe_path = args.lhe_path
run_herwig = args.run_herwig

# get file name from path
if re.match("\w+\.lhe",lhe_path):
    lhe_path = "./"+lhe_path

split_lhe_dir = lhe_path.split("/")
lhe_filename = lhe_path.split("/")[-1]
base_lhe_dir = reduce(lambda x,y: x+"/"+y,[split_lhe_dir[i] for i in range(len(split_lhe_dir) - 1)])

# get root name X of X.lhe
base_filename = lhe_filename.split(".")[-2]

# absolute path to this
abs_lhe_path = getAbsolutePaths(base_lhe_dir, file_pattern=".*/"+lhe_filename)[0]

# check .lhe supplied exists
validateFile(lhe_path)

# open lhe file in read mode
with open(lhe_path, "r") as text_file:

    # store the text as a variable
    file_text = text_file.read()

    # regex match lhe banner
    try:
        lhe_block = re.findall(lhe_banner_pattern, file_text, overlapped=False)

    except AttributeError:
        # not found in the file
        print('No LHE block identified in file: %s' % lhe_path)
        exit(1)

##
#

# get params to construct herwig card
pdf_name = re.findall(pdf_name_pattern, lhe_block[0][0], overlapped=False)[0][0]
pdf_no = re.findall(pdf_num_pattern, lhe_block[0][0], overlapped=False)[0][0]
num_events = re.findall(num_evts_pattern, lhe_block[0][0], overlapped=False)[0][0]

# herwig 7.04 vs 7.12
#LHCGenerator vs EventGenerator
#QCDExtractor vs EEExtractor

print("reading {}...".format(abs_lhe_path))

## base input card
card_text = """#############################################################
# Create an event generator taking the default LHCGenerator #
# as the starting point ...                                 #
#############################################################
cd /Herwig/Generators
# Copy the default LHCGenerator with its settings to a new
# which will be the basis of the one we use for showering:
cp EventGenerator theGenerator

#############################################################
# Create a LH event handler (set up & assigned below) ...   #
#############################################################
cd /Herwig/EventHandlers
library LesHouches.so
create ThePEG::LesHouchesEventHandler theLesHouchesHandler

#############################################################
# Create a LH reader (set up & assigned below) ...          #
#############################################################
cd /Herwig/EventHandlers
create ThePEG::LesHouchesFileReader myReader
set myReader:FileName {lhe_filename}
set myReader:CacheFileName cache.tmp

#############################################################
# Create an LHAPDF (set up & assigned below) ...            #
#############################################################
cd /Herwig/Partons
create ThePEG::LHAPDF thePDFset ThePEGLHAPDF.so

############################################################
# Create a cuts object ...                                 #
############################################################
cd /Herwig/EventHandlers
create ThePEG::Cuts   /Herwig/Cuts/NoCuts

#511
set /Herwig/Particles/B0:Stable Stable
set /Herwig/Particles/Bbar0:Stable Stable
#531
set /Herwig/Particles/B_s0:Stable Stable
set /Herwig/Particles/B_sbar0:Stable Stable
#521
set /Herwig/Particles/B+:Stable Stable
set /Herwig/Particles/B-:Stable Stable
#5122
set /Herwig/Particles/Lambda_b0:Stable Stable
set /Herwig/Particles/Lambda_bbar0:Stable Stable
#5232
set /Herwig/Particles/Xi_b0:Stable Stable
set /Herwig/Particles/Xi_bbar0:Stable Stable
#5132
set /Herwig/Particles/Xi_b-:Stable Stable
set /Herwig/Particles/Xi_bbar+:Stable Stable
#5332
set /Herwig/Particles/Omega_b-:Stable Stable
set /Herwig/Particles/Omega_bbar+:Stable Stable
#5322
set /Herwig/Particles/Xi_b'0:Stable Stable
set /Herwig/Particles/Xi_b'bar0:Stable Stable
#5312
set /Herwig/Particles/Xi_b'-:Stable Stable
set /Herwig/Particles/Xi_b'bar+:Stable Stable
#533
set /Herwig/Particles/B_s*0:Stable Stable
set /Herwig/Particles/B_s*bar0:Stable Stable
#513
set /Herwig/Particles/B*0:Stable Stable
set /Herwig/Particles/B*bar0:Stable Stable
#523
set /Herwig/Particles/B*+:Stable Stable
set /Herwig/Particles/B*-:Stable Stable
#541
set /Herwig/Particles/B_c+:Stable Stable
set /Herwig/Particles/B_c-:Stable Stable
#543
set /Herwig/Particles/B_c*+:Stable Stable
set /Herwig/Particles/B_c*-:Stable Stable
#5114
set /Herwig/Particles/Sigma_b*-:Stable Stable
set /Herwig/Particles/Sigma_b*bar+:Stable Stable
#5212
set /Herwig/Particles/Sigma_b0:Stable Stable
set /Herwig/Particles/Sigma_bbar0:Stable Stable
#5224
set /Herwig/Particles/Sigma_b*+:Stable Stable
set /Herwig/Particles/Sigma_b*bar-:Stable Stable
#5222
set /Herwig/Particles/Sigma_b+:Stable Stable
set /Herwig/Particles/Sigma_bbar-:Stable Stable
#5214
set /Herwig/Particles/Sigma_b*0:Stable Stable
set /Herwig/Particles/Sigma_b*bar0:Stable Stable
#5112
set /Herwig/Particles/Sigma_b-:Stable Stable
set /Herwig/Particles/Sigma_bbar+:Stable Stable
#5324
set /Herwig/Particles/Xi_b*0:Stable Stable
set /Herwig/Particles/Xi_b*bar0:Stable Stable
#5314
set /Herwig/Particles/Xi_b*-:Stable Stable
set /Herwig/Particles/Xi_b*bar+:Stable Stable
#5334
set /Herwig/Particles/Omega_b*-:Stable Stable
set /Herwig/Particles/Omega_b*bar+:Stable Stable


#############################################################
# Setup the LH event handler ...                            #
#############################################################
cd /Herwig/EventHandlers
insert theLesHouchesHandler:LesHouchesReaders 0 myReader
set theLesHouchesHandler:WeightOption 2
set theLesHouchesHandler:PartonExtractor /Herwig/Partons/EEExtractor
#EEExtractor
set theLesHouchesHandler:CascadeHandler /Herwig/Shower/ShowerHandler
set theLesHouchesHandler:HadronizationHandler /Herwig/Hadronization/ClusterHadHandler
set theLesHouchesHandler:DecayHandler /Herwig/Decays/DecayHandler

#############################################################
# Set up the Evolver to veto hard emissions > scalup ...    #
#############################################################
#cd /Herwig/Shower
# MaxTry 100 sets the maximum number of times to try
# showering a given shower tree to 100.
# HardVetoMode Yes to veto emissions with pT greater than pT_max.
# HardVetoScaleSource Read means pT_max comes from hepeup.SCALUP.
# This is what you need to set _along_with_ HardVetoMode Yes in
# the case of Powheg external events _AND_ mc@nlo.
# MeCorrMode No turns off ME corrs.
#set Evolver:MaxTry               100
#set Evolver:HardVetoMode         Yes
#set Evolver:HardVetoScaleSource  Read
#set Evolver:HardVetoReadOption   PrimaryCollision
#set Evolver:MECorrMode           No

#############################################################
# Set up the LH reader ...                                  #
#############################################################
cd /Herwig/EventHandlers
# set myReader:WeightWarnings    false
set myReader:MomentumTreatment      RescaleEnergy
set myReader:Cuts  /Herwig/Cuts/NoCuts

#############################################################
# Set up the LHAPDF ...                                     #
#############################################################
cd /Herwig/Partons
# Don't try and find PDF index out from the LH file ...
set /Herwig/EventHandlers/myReader:InitPDFs false
# set /Herwig/EventHandlers/myReader:InitPDFs true
# Instead set them explicitly here:
#set thePDFset:PDFName       {pdf_name}
#set thePDFset:RemnantHandler  HadronRemnants
#set /Herwig/EventHandlers/myReader:PDFA thePDFset
#set /Herwig/EventHandlers/myReader:PDFB thePDFset
#set /Herwig/Particles/p+:PDF    thePDFset
#set /Herwig/Particles/pbar-:PDF thePDFset
# The PDF for beam particles A/B - overrides particle's own PDF above
#set /Herwig/Shower/ShowerHandler:PDFA thePDFset
#set /Herwig/Shower/ShowerHandler:PDFB thePDFset

#set /Herwig/Particles/tau+:Stable Stable
#set /Herwig/Particles/tau-:Stable Stable

#set /Herwig/Decays/DecayHandler:MaxLifeTime 0.3*mm
#set /Herwig/Decays/DecayHandler:LifeTimeOption Average

####################################################
# Set up the generator ...                         #
####################################################
cd /Herwig/Generators
set theGenerator:EventHandler /Herwig/EventHandlers/theLesHouchesHandler
set theGenerator:NumberOfEvents {num_events}
set theGenerator:RandomNumberGenerator:Seed 31122001
set theGenerator:PrintEvent     10
set theGenerator:MaxErrors      10000
# Stuff for rivet (optional!):
insert theGenerator:AnalysisHandlers 0 /Herwig/Analysis/HepMCFile
set /Herwig/Analysis/HepMCFile:PrintEvent 1000000
set /Herwig/Analysis/HepMCFile:Format     GenEvent
set /Herwig/Analysis/HepMCFile:Units      GeV_mm
set /Herwig/Analysis/HepMCFile:Filename   {sample_name}.hepmc

# create ThePEG::RivetAnalysis Rivet RivetAnalysis.so
# insert Rivet:Analyses 0 MC_GENERIC
# insert EventGenerator:AnalysisHandlers 0 Rivet

#############################################################
# The basic consistency analysis checks charge and momentum #
# conservation. Also it will print zillions of errors to    #
# the screen telling of quarks in the final state, unless   #
# you disable that check with the following switch ...      #
#############################################################
cd /Herwig/Analysis
set Basics:CheckQuark 0
# No need for this unless doing a quick parton level analysis!
# If doing hadron level comment the previous line out.

#########################################################
# Option to off shower / hadronization / decays / MPI.  #
#########################################################
cd /Herwig/EventHandlers
 set theLesHouchesHandler:CascadeHandler        NULL
 set theLesHouchesHandler:HadronizationHandler  NULL
 set theLesHouchesHandler:DecayHandler          NULL
# The handler for multiple parton interactions
 set /Herwig/Shower/ShowerHandler:MPIHandler    NULL

################
# Save the run #
################
cd /Herwig/Generators
saverun {sample_name} theGenerator
""".format(
lhe_filename = lhe_path,
sample_name = base_filename,
num_events = num_events,
pdf_name = "NNPDF23_nnlo_as_0118_qed"
)

#print(card_text)

# create a filename to hold modified card
output_filename = "herwig_input_" + base_filename + ".in"

# define also the name of the .run produced by Herwig read
run_filename = base_filename + ".run"

with open(output_filename, "w") as text_file:
    text_file.write(card_text)

# run herwig if requested
if run_herwig:
    subprocess.call(["Herwig", "read", output_filename])
    validateFile(run_filename)
    subprocess.call("Herwig", "run", run_filename)
#Herwig read herwig_mod_uubarttbarOAV25em3500k.in &&
#Herwig run uubarttbarOAV25em3500k.run &&
