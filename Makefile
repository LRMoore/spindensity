## Makefile to build and install Rivet analyses

CC=g++
FLAGS= -Wall -Wextra
CFLAGS=-std=c++11 -m64 -fPIC -pg -I$(RIVETINCDIR) -O3 -march=native -pedantic

INCDIR=-I$(PWD)/include -I$(shell root-config --cflags)
LIBDIR:=-L$(shell rivet-config --libdir) -L$(shell root-config --libdir --libs) -lHepMC
PREFIX:=$(shell rivet-config --prefix)
RIVETINCDIR:=$(shell rivet-config --includedir)
LDFLAGS:=$(shell rivet-config --ldflags)


## Get the first part of the string passed
ANALYSISPATH = $(firstword $(subst :, ,$1))

Rivettt_Pol_Ana.so: src/ttbar_Dilepton_SpinDensity.cc
	rivet-buildplugin Rivetttbar_Dilepton_SpinDensity.so src/ttbar_Dilepton_SpinDensity.cc $(CFLAGS) $(INCDIR) $(LIBDIR)

install: Rivetttbar_Dilepton_SpinDensity.so
	cp -f Rivetttbar_Dilepton_SpinDensity.so $(call ANALYSISPATH, $(RIVET_ANALYSIS_PATH))

clean:
	rm -f *.o  *.so
