###
###
# Contents:
###
###

#
## ./
#

- makefile for rivet analysis (assumes $RIVET_ANALYSIS_PATH set and rivet installed)

#
## /src/
#

- Rivet analysis for constructing spin density coefficients from mean values of
 cos(theta)^(t/t~)(i) (pol) & cos(theta)^(t)(i) * cos(theta)^(t~)(j) (corr) histograms

- uses partonic top four-momenta from event record to define rest frames
 produces source histogram output in .yoda file, and 'coeffs.dat' file with numerical
 coefficients and their MC errors

#
## /generate_events/
#

- simple madgraph .cmd macros to produce LO tt~ (decay chain -> dilepton) in the SM for testing

- mg5 macro to configure run cards to set to 8 tev, switch off cuts, shower etc.
 generated 100k unw. events

- madspin card for setting leptonic decay modes (if choose to use madspin as with NLO)

#
## /Plot_Configs/
#

- plot configuration file for rivet histograms. currently just switches of LogY scale
 for distributions

#
## /bin/
#

- template python script to generate herwig (>7.1.x) .in file for converting .lhe file
 to hepmc (ie 'showering' allowing final state quarks and setting everything to
 stable so can define observables from parton four-momenta)

- run with python bin/lhe2hepmc.py (-r) my_events.lhe
 where -r will optionally use Herwig to read the .in card and run

#
## /lhef2hepmc/
#

- dated script to convert .lhe directly to .hepmc
 works only for lhef v1.0, so not used here

###
###
# Instructions for different steps:
###
###

# generate madgraph events (example)
- <mg_build>/bin/mg5_aMC /path/to/create_tt_dilept_process_dir.cmd

# configure cards to switch off cuts, set 8tev etc (example)
- cd tt_dilept_lo_sm
- ./bin/madevent configure_cards_LO_SM_8tev_nocuts.cmd
- ./bin/generate_events

# convert lhef to hepmc:
 python ./bin/lhe2hepmc.py (-r) my_events.lhe

# make analysis:
- make && make install

# run analysis:
- rivet -a ttbar_Dilepton_SpinDensity -H <histogram_output_filename>.yoda <input_sample_filename>.hepmc
 (produces coeffs.dat once finished running)

# make plots
- rivet-mkhtml -c Plot_Configs/plotconfig.file -o <plot_directory_name> <histogram_output_filename>.yoda
