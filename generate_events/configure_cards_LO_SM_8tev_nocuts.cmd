# use as:  ./bin/madevent setup_100k_events_umeps.cmd
#
# move to card editing mode (configure run, but don't generate events yet)
edit_cards
# set auto-shower off so can run pythia separately
shower=OFF
# set detector sim off
detector=OFF
# set madanalysis off
analysis=OFF
# set reweight on so can regenerate samples with varied param choices
reweight=OFF
# move to editing param/runcards
done
#
set run_tag tt_dilept_lo_sm
# set 100k unweighted_events
set nevents 100000
# will switch off ickkw since not merging
set ickkw 0
# beam energy to 8tev
set ebeam1 4000
set ebeam2 4000
#
#*******************************
# remove parton level cuts     *
#*******************************
#*********************************************************************
# Minimum and maximum pt's (for max, -1 means no cut)                *
#*********************************************************************
set ptj 0.0
set ptb 0.0
set pta 0.0
set ptl 0.0
set misset 0.0
set ptjmax -1.0
set ptbmax -1.0
set ptamax -1.0
set ptlmax -1.0
set missetmax -1.0
#*********************************************************************
# Minimum and maximum E's (in the center of mass frame)              *
#*********************************************************************
set ej 0.0
set eb 0.0
set ea 0.0
set el 0.0
set ejmax -1.0
set ebmax -1.0
set eamax -1.0
set elmax -1.0
#*********************************************************************
# Maximum and minimum absolute rapidity (for max, -1 means no cut)   *
#*********************************************************************
 set etaj -1.0
 set etab -1.0
 set etaa -1.0
 set etaa -1.0
 set etal -1.0
 set etajmin 0.0
 set etabmin 0.0
 set etaamin 0.0
 set etalmin 0.0
#*********************************************************************
# Minimum and maximum DeltaR distance                                *
#*********************************************************************
set drjj 0.0
set drbb 0.0
set drll 0.0
set draa 0.0
set drbj 0.0
set draj 0.0
set drjl 0.0
set drab 0.0
set drbl 0.0
set dral 0.0
set drjjmax -1.0
set drbbmax -1.0
set drllmax -1.0
set draamax -1.0
set drbjmax -1.0
set drajmax -1.0
set drjlmax -1.0
set drabmax -1.0
set drblmax -1.0
set dralmax -1.0
#*********************************************************************
# Minimum and maximum invariant mass for pairs                       *
# WARNING: for four lepton final state mmll cut require to have      *
#          different lepton masses for each flavor!                  *
#*********************************************************************
set mmjj 0.0
set mmbb 0.0
set mmaa 0.0
set mmll 0.0
set mmjjmax 0.0
set mmbbmax 0.0
set mmaamax 0.0
set mmllmax 0.0
#*********************************************************************
# Minimum and maximum invariant mass for all letpons                 *
#*********************************************************************
set mmnl 0.0
set mmnlmax -1.0
#*********************************************************************
# Minimum and maximum pt for 4-momenta sum of leptons                *
#*********************************************************************
set ptllmin 0.0
set ptllmax -1.0
#*********************************************************************
# Inclusive cuts                                                     *
#*********************************************************************
set ptheavy 0.0
set xptj 0.0
set xptb 0.0
set xpta 0.0
set xptl 0.0
#*********************************************************************
# Control the pt's of the jets sorted by pt                          *
#*********************************************************************
set ptj1min 0.0
set ptj2min 0.0
set ptj3min 0.0
set ptj4min 0.0
set ptj1max -1.0
set ptj2max -1.0
set ptj3max -1.0
set ptj4max -1.0
set cutuse 0
#*********************************************************************
# Control the pt's of leptons sorted by pt                           *
#*********************************************************************
set ptl1min 0.0
set ptl2min 0.0
set ptl3min 0.0
set ptl4min 0.0
set ptl1max -1.0
set ptl2max -1.0
set ptl3max -1.0
set ptl4max -1.0
#*********************************************************************
# Control the Ht(k)=Sum of k leading jets                            *
#*********************************************************************
set htjmin 0.0
set htjmax -1.0
set ihtmin 0.0
set ihtmax -1.0
set ht2min 0.0
set ht3min 0.0
set ht4min 0.0
set ht2max -1.0
set ht3max -1.0
set ht4max -1.0
#***********************************************************************
# Photon-isolation cuts, according to hep-ph/9801442                   *
# When ptgmin=0, all the other parameters are ignored                  *
# When ptgmin>0, pta and draj are not going to be used                 *
#***********************************************************************
set ptgmin 0.0
#*********************************************************************
# WBF cuts                                                           *
#*********************************************************************
set xetamin 0.0
set deltaeta 0.0
#***********************************************************************
# Turn on either the ktdurham or ptlund cut to activate                *
# CKKW(L) merging with Pythia8 [arXiv:1410.3012, arXiv:1109.4829]      *
#***********************************************************************
set ktdurham -1.0
set ptlund -1.0
#*********************************************************************
# maximal pdg code for quark to be considered as a light jet         *
# (otherwise b cuts are applied)                                     *
#*********************************************************************
set maxjetflavor 4
#*********************************************************************
#
#*********************************************************************
# Store info for systematics studies                                 *
# WARNING: Do not use for interference type of computation           *
#*********************************************************************
set use_syst False
# exit setting up Cards
done
